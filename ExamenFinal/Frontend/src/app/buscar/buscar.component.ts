import { Component, OnInit } from '@angular/core';
import {ApiService} from "../ApiService";
import {Usuario, RespuestaUsuario, RespuestaBusquedaUsuario} from "../interfaces";
@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.scss']
})
export class BuscarComponent implements OnInit {
  id_usuario:number = 0;
  nombres: string = '';
  apellidos: string = '';
  correo: string = '';
  administrador: string = '';
  clave: string = '';
  listaUsuarios: Usuario[] =[];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
  }
  buscarUsuario(): void {
    console.log(this.nombres);
    console.log(this.apellidos);
    console.log(this.correo);

    const usuario: Usuario = {
      id_usuario:0,
      nombres: this.nombres,
      apellidos: this.apellidos,
      correo: this.correo,
      administrador: '',
      clave: ''
  };
this.api.mostrarUsuario(usuario).subscribe(data => {
  this.listaUsuarios = data.lista;
});
}
}
