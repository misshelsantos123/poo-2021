import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { retry, catchError } from 'rxjs/operators';
import {RespuestaBusquedaUsuario, RespuestaUsuario, Usuario} from "./interfaces";

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;charset=utf-8'
    })
  };

  errorHandl(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      errorMessage = error.error.message;
    } else {
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }

  constructor(private http: HttpClient) {
  }

  obtenerUsuario(data:Usuario): Observable<Usuario> {
    return this.http.post<Usuario>('http://127.0.0.1:8080/obtener-usuario', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  actualizarUsuario(data:Usuario): Observable<Usuario> {
    return this.http.post<Usuario>('http://127.0.0.1:8080/actualizar-usuario', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
  mostrarUsuario(data:Usuario): Observable<RespuestaBusquedaUsuario> {

    return this.http.post<RespuestaBusquedaUsuario>('http://127.0.0.1:8080/mostrar-empleado', data, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandl)
      );
  }
}
