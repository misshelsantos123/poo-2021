export interface Usuario {
  id_usuario: number;
  nombres: string;
  apellidos: string;
  correo:string;
  administrador:string;
  clave:string;
}
export interface RespuestaUsuario {
  nombres: string;
  apellidos: string;
  clave:string;
}
export interface RespuestaBusquedaUsuario{
  lista: Usuario[];
}


