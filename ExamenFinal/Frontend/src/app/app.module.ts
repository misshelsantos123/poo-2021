import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import { BuscarComponent } from './buscar/buscar.component';
import { ActualizarComponent } from './actualizar/actualizar.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    BuscarComponent,
    ActualizarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
    //StoreModule.forRoot("sesionUsuario: sesionUsuarioReducer")
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
