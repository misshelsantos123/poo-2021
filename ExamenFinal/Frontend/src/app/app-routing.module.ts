import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {BuscarComponent} from "./buscar/buscar.component";

const routes: Routes = [{path:"login", component: LoginComponent},{path:"buscar", component:BuscarComponent}];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
