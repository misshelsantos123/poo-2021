import { Component, OnInit } from '@angular/core';
import {ApiService} from "../ApiService";
import {Usuario} from "../interfaces";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  mostrar(): void{
    let menu:any = document.querySelector('#mobile-links');
    menu.classList.toggle('hidden');
    console.log("Mensaje enviado")
  }
  correo: string = "";
  clave: string = "";
  mensaje: string = "";

  ngOnInit(): void {
  }
  constructor(private api: ApiService, private ruteador:Router) { }
  ingresar(): void{
    const usuario: Usuario = {
      id_usuario:0,
      nombres: '',
      apellidos: '',
      correo: this.correo,
      administrador: '',
      clave: this.clave
    }
    this.api.obtenerUsuario(usuario).subscribe( respuesta => {
      if(respuesta.id_usuario != 0){
        this.mensaje = "Ingresaste";
        this.ruteador.navigateByUrl("principal");
      }
      else{
        this.mensaje = "No ingresaste";
      }
    });
  }

}
