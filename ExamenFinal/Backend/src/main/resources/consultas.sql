USE poo;
CREATE TABLE usuario(
                        id_usuario NUMERIC(9)  , -- PK
                        nombres VARCHAR(100),
                        apellidos VARCHAR(100),
                        correo VARCHAR(100),
                        administrador CHAR(1),
                        clave VARCHAR(200)
);
select *
from usuario;

insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
values (1,'juan','dias','juan@gmail.com',0,null);
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
VALUES (2,'pedro','perez','juan123@gmail.com',0,null);
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
VALUES (3,'misshel','santos','misshel@gmail.com',1,'123');
insert into usuario(id_usuario, nombres, apellidos, correo, administrador, clave)
VALUES (4,'carlos','perez','carlos@gmail.com',0,null);

SELECT *
FROM usuario
WHERE correo="misshel@gmail.com" and clave = "123";

UPDATE usuario
SET nombres='danery' , correo='danery@gmail.com'
WHERE id_usuario=3 and administrador<>null;
select e.nombres name, e.apellidos apellido, e.correo correo
from usuario e;


