package com.example.backend.controlador;

import com.example.backend.dto.RespuestaUsuario;
import com.example.backend.dto.Usuario;
import com.example.backend.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})

public class Controlador {
    @Autowired
    private Servicio servicio;
    @RequestMapping(value = "/obtener-usuario", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    Usuario obtenerUsuario(@RequestBody Usuario usuario){
        return servicio.obtenerUsuario(usuario);
    };

    @RequestMapping(value = "/actualizar-usuario", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    Usuario actualizarUsuario(@RequestBody Usuario usuario){
        return servicio.actualizarUsuario(usuario);
    };

    @RequestMapping(value = "/mostrar-usuario", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    RespuestaUsuario mostrarUsuario(@RequestBody Usuario usuario){
        return servicio.mostrarUsuario(usuario);
    };
}
