package com.example.backend.dao;

import com.example.backend.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void crearConexion() {
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion() {
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Usuario obtenerUsuario(Usuario usuario) {
        crearConexion();
        String sql = " SELECT nombres, apellidos\n" +
                " FROM usuario\n" +
                " WHERE correo=? and clave = ? ";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario.getCorreo());
            sentencia.setString(2, usuario.getClave());
            ResultSet resultado = sentencia.executeQuery();
                while (resultado.next()) {
                    usuario.setNombres(resultado.getString("nombres"));
                    usuario.setApellidos(resultado.getString("apellidos"));
                }
                resultado.close();
                sentencia.close();
                cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) {
        crearConexion();
        String sql = " UPDATE usuario\n"+
                " SET nombres=? , correo=?"+
                " WHERE id_usuario=? ";
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, usuario.getId_usuario());
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                usuario.setNombres(resultado.getString("nombres"));
                usuario.setApellidos(resultado.getString("apellidos"));
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return usuario;
    }

    @Override
    public RespuestaUsuario mostrarUsuario(Usuario usuario) {
        RespuestaUsuario respuestaUsuario= new RespuestaUsuario();
        respuestaUsuario.setLista(new ArrayList<>());
        boolean primero = true;
        String sql= " select e.nombres name, e.apellidos apellido, e.correo correo " +
                " from usuario e\n";

        if(usuario.getNombres() != null && !usuario.getNombres().equals(0)){
            sql += " where e.nombres=?";
            primero = false;
        }
        if(usuario.getApellidos() != null && !usuario.getApellidos().equals(0)){
            if(primero){
                sql += " where e.apellidos like ?";
            }
            else{
                sql += " and e.apellidos like ?";
            }
        }
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            int indice = 1;
            if(usuario.getNombres() != null && !usuario.getNombres().equals(0)){
                sentencia.setString(indice, usuario.getNombres());
                indice++;
            }
            if(usuario.getApellidos() != null && !usuario.getApellidos().equals(0)){
                sentencia.setString(indice, usuario.getApellidos());
            }

            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                Usuario e = new Usuario();
                e.setNombres(resultado.getString("nombres"));
                e.setApellidos(resultado.getString("apellidos"));
                e.setCorreo(resultado.getString("correo"));
                respuestaUsuario.getLista().add(e);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return respuestaUsuario;
    }
}
