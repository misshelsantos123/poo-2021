package com.example.backend.dao;

import com.example.backend.dto.RespuestaUsuario;
import com.example.backend.dto.Usuario;

import java.sql.SQLException;

public interface Dao {
    public Usuario obtenerUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
    public RespuestaUsuario mostrarUsuario (Usuario usuario);
}
