package com.example.backend.servicio;

import com.example.backend.dto.*;

public interface Servicio {
    public Usuario obtenerUsuario(Usuario usuario);
    public Usuario actualizarUsuario(Usuario usuario);
    public RespuestaUsuario mostrarUsuario (Usuario usuario);
}
