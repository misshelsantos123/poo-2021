package com.example.backend.servicio;

import com.example.backend.dao.Dao;
import com.example.backend.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;

    @Override
    public Usuario obtenerUsuario(Usuario usuario) {
        return dao.obtenerUsuario(usuario);
    }

    @Override
    public Usuario actualizarUsuario(Usuario usuario) {
        return dao.actualizarUsuario(usuario);
    }

    @Override
    public RespuestaUsuario mostrarUsuario(Usuario usuario) {
        return dao.mostrarUsuario(usuario);
    }
}
