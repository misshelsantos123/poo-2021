package LAB8;
import java.sql.*;
public class App
{
    public static void main( String[] args ) throws Exception{
        Class.forName("org.mariadb.jdbc.Driver");
        Connection conexion = DriverManager.getConnection("jdbc:mariadb://127.0.0.1:3306/poo","root","MisshelSantos:3");
        Statement sentencia = conexion.createStatement();
        String sql = "SELECT c.nombres,pr.nombre,SUM(d.cantidad) cantidad" +
                " FROM cliente c JOIN pedido p ON (c.id_cliente = p.id_cliente)" +
                " JOIN detalle_producto d ON (p.id_pedido = d.id_pedido)" +
                " JOIN producto pr ON(d.id_producto = pr.id_producto)" +
                " GROUP BY c.nombres, pr.nombre";
        ResultSet resultado = sentencia.executeQuery(sql);
        while(resultado.next()){
            System.out.println("NombreProducto: " + resultado.getString("nombre"));
            System.out.println("NombreCliente: " + resultado.getString("nombres"));

            System.out.println("Cantidad: " + resultado.getInt("cantidad"));
        }
        resultado.close();
        sentencia.close();
        conexion.close();
    }
}
