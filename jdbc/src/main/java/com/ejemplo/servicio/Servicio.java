package com.ejemplo.servicio;

import com.ejemplo.dto.Departamento;

import java.util.List;

public interface Servicio {
    void agregarDepartamento(Departamento departamento);
    List<Departamento> obtenerDepartamentos();

}
