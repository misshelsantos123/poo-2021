package com.ejemplo.dto;

public class Departamento {
    private Integer codigo;
    private  String nombre;
    private Integer codigoLocacion;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigoLocacion() {
        return codigoLocacion;
    }

    public void setCodigoLocacion(Integer codigoLocacion) {
        this.codigoLocacion = codigoLocacion;
    }
}
