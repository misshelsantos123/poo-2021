package VentaCelulares;

public class Main {
    public static void main(String[] args) {
        Venta venta =new Venta(-150.0d); //CELULAR DE 150 SOLES
        System.out.println("Inicio del proceso");
        try{
            venta.setImpuesto();
        }
        catch (ExcepcionNegativo e){
            e.printStackTrace();
        }
        finally{
            System.out.println("Impuesto: "+venta.getImpuesto());
        }
        System.out.println("Fin del proceso"); //PARA UN GRUPO DE VENTA DE CELULARES
    }
}
