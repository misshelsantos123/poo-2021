package VentaCelulares;

public class Venta implements CalculoImpuestos {
    protected double precio;
    protected double impuesto;

    public Venta(double precio) {
        this.precio = precio;
        this.impuesto = precio * CalculoImpuestos.impuesto;
    }

    public void setImpuesto() throws ExcepcionNegativo{
        if(this.precio<0){
            throw new ExcepcionNegativo();
        }
    }

    @Override
    public double getImpuesto() {
        return this.impuesto;
    }
}
