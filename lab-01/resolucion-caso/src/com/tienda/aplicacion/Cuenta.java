package com.tienda.aplicacion;

public class Cuenta {
    private Venta[] ventas;

    public static void main(String[] args) {
        Cuenta p = new Cuenta();
        p.ventas = new Venta[2];

        Producto[] productos1 = new Producto[2];

        Producto pro11 = new Producto();// Se crea primero un objeto
        pro11.setCodigo(101);
        pro11.setCosto(10.5);
        pro11.setNombre("Toalla");
        productos1[0] = pro11;// El objeto creado se asigna a la primera posicion de la lista luego de
        // definirse

        productos1[1] = new Producto();// Se crea por referencia el segundo objeto y luego de define
        productos1[1].setCodigo(102);
        productos1[1].setCosto(5.1);
        productos1[1].setNombre("Jabon");

        p.ventas[0] = new Venta();
        p.ventas[0].setProductos(productos1);

        Producto[] productos2 = new Producto[2];

        Producto pro21 = new Producto();// Se crea primero un objeto
        pro21.setCodigo(201);
        pro21.setCosto(30.5);
        pro21.setNombre("Serrucho");
        productos2[0] = pro21;// El objeto creado se asigna a la primera posicion de la lista luego de
        // definirse

        productos2[1] = new Producto();// Se crea por referencia el segundo objeto y luego de define
        productos2[1].setCodigo(202);
        productos2[1].setCosto(25.7);
        productos2[1].setNombre("Martillo");

        p.ventas[1] = new Venta();
        p.ventas[1].setProductos(productos2);

        System.out.println("El total de las ventas es: " + p.calcularTotal());
        System.out.println("La mayor de las ventas es: " + p.hallarMayor());
        System.out.println("La menor de las ventas es: " + p.hallarMenor());
    }

    public Double calcularTotal() {
        Double total = 0D;
        for (int i = 0; i < this.ventas.length; i++) {
            for (int j = 0; j < this.ventas[i].getProductos().length; j++) {
                total = total + this.ventas[i].getProductos()[j].getCosto();
            }
        }
        return total;
    }

    public Double hallarMayor() {
        Double mayor = 0D;
        Double suma;
        boolean valor = true;
        for (int i = 0; i < this.ventas.length; i++) {
            suma = 0D;
            for (int j = 0; j < this.ventas[i].getProductos().length; j++) {
                suma = suma + this.ventas[i].getProductos()[j].getCosto();
            }
            if (valor) {
                mayor = suma;
                valor = false;
            } else if (suma > mayor) {
                mayor = suma;
            }
        }
        return mayor;
    }

    public Double hallarMenor() {
        Double menor = 0D;
        Double suma;
        boolean valor = true;
        for (int i = 0; i < this.ventas.length; i++) {
            suma = 0D;
            for (int j = 0; j < this.ventas[i].getProductos().length; j++) {
                suma = suma + this.ventas[i].getProductos()[j].getCosto();
            }
            if (valor) {
                menor = suma;
                valor = false;
            } else if (suma < menor) {
                menor = suma;
            }
        }
        return menor;
    }
}
