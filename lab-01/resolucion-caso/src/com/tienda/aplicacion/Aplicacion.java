package com.tienda.aplicacion;

public class Aplicacion {
    public static void main(String[] args) {
        System.out.println("Bienvenido a VS code");
        String arreglo[] = { "Johan", "Alvaro", "Carlos", "Vanessa" };
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println(arreglo[i]);
        }
        Aplicacion a = new Aplicacion();
        a.ordenar(arreglo);
        for (int i = 0; i < arreglo.length; i++) {
            System.out.println(arreglo[i]);
        }
    }

    public void ordenar(String[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].compareTo(array[j]) > 0) {
                    String temporal = array[i];
                    array[i] = array[j];
                    array[j] = temporal;
                }
            }
        }
    }
}
