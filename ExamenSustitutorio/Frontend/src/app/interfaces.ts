export interface Estudiante {
  id_estudiante: number;
  nombres: string;
  apellidos: string;
  correo:string;
  clave:string;
}

export interface Curso {
  id_curso: number;
  nombre: string;
  precio: number;
  fecha_inicio:string;
  fecha_fin:string;
}

export interface IncripcionCurso {
  id_curso: number;
  id_estudiante: number;
  fecha_inscripcion:string;
}

export interface RespuestaBusquedaCurso{
  lista: Curso[];
}



