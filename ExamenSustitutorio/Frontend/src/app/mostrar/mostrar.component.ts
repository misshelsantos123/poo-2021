import { Component, OnInit } from '@angular/core';
import {ApiService} from "../ApiService";
import {Curso, RespuestaBusquedaCurso} from "../interfaces";
@Component({
  selector: 'app-mostrar',
  templateUrl: './mostrar.component.html',
  styleUrls: ['./mostrar.component.scss']
})
export class MostrarComponent implements OnInit {
  id_curso: number = 0;
  nombre: string = '';
  precio: number = 0;
  fecha_inicio: string = '';
  fecha_fin: string = '';
  listaCurso: Curso[] =[];
  constructor(private api: ApiService) { }

  ngOnInit(): void {
      const curso: Curso = {
      id_curso:this.id_curso,
      nombre: this.nombre,
      precio: this.precio,
      fecha_inicio: this.fecha_inicio,
      fecha_fin: this.fecha_fin
    };
    this.api.mostrarCurso(curso).subscribe(data => {
      this.listaCurso = data.lista;
    });
  }
}
