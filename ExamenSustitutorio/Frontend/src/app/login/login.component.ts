import { Component, OnInit } from '@angular/core';
import {ApiService} from "../ApiService";
import {Estudiante, RespuestaBusquedaCurso, Curso} from "../interfaces";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  id_estudiante: number = 0;
  correo: string = "";
  clave: string = "";
  listaCursos: Curso[] =[];
  mensaje: string = "";

  ngOnInit(): void {
  }
  constructor(private api: ApiService, private ruteador:Router) { }
  ingresar(): void{
    const estudiante: Estudiante = {
      id_estudiante:this.id_estudiante,
      nombres: '',
      apellidos: '',
      correo: this.correo,
      clave: this.clave
    }
    this.api.obtenerEstudiante(estudiante).subscribe( data => {
      if(data.lista != []){
        this.mensaje = "Ingresaste";
        this.listaCursos = data.lista;
      }
      else{
        this.mensaje = "No ingresaste";
      }
    });
  }

}
