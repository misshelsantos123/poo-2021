package com.example.backend.controlador;

import com.example.backend.dto.*;
import com.example.backend.servicio.Servicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = {"*"})

public class Controlador {
    @Autowired
    private Servicio servicio;

    @RequestMapping(value = "/obtener-estudiante", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    RespuestaCurso obtenerEstudiante(@RequestBody Estudiante estudiante){
        return servicio.obtenerEstudiante(estudiante);
    };

    @RequestMapping(value = "/mostrar-curso", method = RequestMethod.POST,
            consumes = "application/json;charset=utf-8",
            produces = "application/json;charset=utf-8"
    )
    public @ResponseBody
    RespuestaCurso mostrarCurso(Curso curso){
        return servicio.mostrarCurso(curso);
    };
}
