package com.example.backend.dao;

import com.example.backend.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;

    private void crearConexion() {
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private void cerrarConexion() {
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public RespuestaCurso obtenerEstudiante(Estudiante estudiante) {
        RespuestaCurso respuestaCurso = new RespuestaCurso();
        respuestaCurso.setLista(new ArrayList<>());
        String sql = " SELECT c.nombre,c.precio,c.fecha_inicio,c.fecha_fin\n " +
                " FROM estudiante\n" +
                " join inscripcion_curso ic on estudiante.id_estudiante = ic.id_estudiante\n" +
                " join curso c on ic.id_curso = c.id_curso\n" +
                " WHERE correo=? and clave = ? ";
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, estudiante.getCorreo());
            sentencia.setString(2, estudiante.getClave());
            ResultSet resultado = sentencia.executeQuery();
                while (resultado.next()) {
                    Curso curso = new Curso();
                    curso.setNombre(resultado.getString("nombre"));
                    curso.setPrecio(resultado.getDouble("precio"));
                    curso.setFecha_inicio(resultado.getString("fecha_inicio"));
                    curso.setFecha_fin(resultado.getString("fecha_fin"));
                    respuestaCurso.getLista().add(curso);
                }
                resultado.close();
                sentencia.close();
                cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return respuestaCurso;
    }

    @Override
    public RespuestaCurso mostrarCurso(Curso curso) {
        RespuestaCurso respuestaCurso = new RespuestaCurso();
        respuestaCurso.setLista(new ArrayList<>());
        String sql = " select nombre, precio, fecha_inicio, fecha_fin " +
                " from curso \n";
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                Curso c = new Curso();
                c.setNombre(resultado.getString("nombre"));
                c.setPrecio(resultado.getDouble("precio"));
                c.setFecha_inicio(resultado.getString("fecha_inicio"));
                c.setFecha_fin(resultado.getString("fecha_fin"));
                respuestaCurso.getLista().add(c);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return respuestaCurso;
    }
}
