package com.example.backend.dto;

import lombok.Data;

@Data

public class Curso {
    private Integer id_curso;
    private String nombre;
    private Double precio;
    private String fecha_inicio;
    private String fecha_fin;
}
