package com.example.backend.dao;

import com.example.backend.dto.*;

public interface Dao {
    public RespuestaCurso obtenerEstudiante(Estudiante estudiante);
    public RespuestaCurso mostrarCurso (Curso curso);
}
