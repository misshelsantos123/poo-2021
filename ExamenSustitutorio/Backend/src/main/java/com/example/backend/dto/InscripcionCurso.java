package com.example.backend.dto;

import lombok.Data;

@Data

public class InscripcionCurso {
    private Integer id_curso;
    private Integer id_estudiante;
    private String fecha_inscripcion;
}
