package com.example.backend.servicio;

import com.example.backend.dto.*;

public interface Servicio {
    public RespuestaCurso obtenerEstudiante(Estudiante estudiante);
    public RespuestaCurso mostrarCurso (Curso curso);
}
