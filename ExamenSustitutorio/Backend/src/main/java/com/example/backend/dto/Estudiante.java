package com.example.backend.dto;

import lombok.Data;

@Data
public class Estudiante {
    private Integer id_estudiante;
    private String nombres;
    private String apellidos;
    private String correo;
    private String clave;
}
