USE poo;
CREATE TABLE estudiante(
                        id_estudiante NUMERIC(9),
                        nombres VARCHAR(200),
                        apellidos VARCHAR(200),
                        correo VARCHAR(200),
                        clave VARCHAR(200)
);
CREATE TABLE curso(
                        id_curso numeric(9),
                        nombre varchar(200),
                        precio numeric(9,2),
                        fecha_inicio varchar(10),
                        fecha_fin varchar(10)
);
CREATE TABLE inscripcion_curso(
                        id_curso numeric(9),
                        id_estudiante numeric(9),
                        fecha_inscripcion varchar(10)
);

insert into estudiante(id_estudiante, nombres, apellidos, correo, clave)
values (1,'juan','dias','juan@gmail.com','123');
insert into estudiante(id_estudiante, nombres, apellidos, correo, clave)
VALUES (2,'pedro','perez','juan123@gmail.com','1234');
insert into estudiante(id_estudiante, nombres, apellidos, correo, clave)
VALUES (3,'misshel','santos','misshel@gmail.com', '1');
insert into estudiante(id_estudiante, nombres, apellidos, correo, clave)
VALUES (4,'carlos','perez','carlos@gmail.com','12');

insert into curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (1,'POO',700.50,'20/03/2021','02/08/2021');
insert into curso(id_curso, nombre, precio, fecha_inicio, fecha_fin)
VALUES (2,'ESTADISTICA',600.50,'22/04/2021','02/07/2021');

INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (1,1,'01/08/2021');
INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (1,2,'01/08/2021');
INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (1,3,'01/08/2021');
INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (1,4,'01/08/2021');

INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (2,1,'01/08/2021');
INSERT INTO inscripcion_curso(ID_CURSO, ID_ESTUDIANTE, FECHA_INSCRIPCION)
VALUES  (2,2,'01/08/2021');


SELECT c.nombre,c.precio,c.fecha_inicio,c.fecha_fin
FROM estudiante
join inscripcion_curso ic on estudiante.id_estudiante = ic.id_estudiante
join curso c on ic.id_curso = c.id_curso
WHERE correo='juan@gmail.com' and clave = '123';

SELECT nombre, precio, fecha_inicio, fecha_fin
FROM CURSO;



