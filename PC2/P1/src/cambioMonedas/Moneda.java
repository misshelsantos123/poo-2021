package cambioMonedas;

public class Moneda implements monedaAdolares,dolaresSsoles{
    private double moneda;

    public Moneda(double moneda) {
        this.moneda = moneda;
    }

    @Override
    public void cambiarAdolares() {
        this.moneda=moneda*monedaAdolares.cambio;
    }

    @Override
    public void cambiarAsoles() {
        this.moneda=moneda*dolaresSsoles.cambio;
    }

    public double getMoneda() {
        return moneda;
    }

}
