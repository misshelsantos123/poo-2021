package BuenaSalud;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Personas {
    List<Persona> lista;
    Map<Persona, Integer> mapa;

    public Personas() {
        this.lista = new ArrayList<Persona>();
        this.mapa = new HashMap<Persona, Integer>();
    }
    public void procesarMapa(){
        llenarMapa();
        mostrarDatosMapa();
    }
    private void mostrarDatosMapa(){
        for (Persona llave: mapa.keySet()) {
            System.out.println("Key: "+ llave.nombre1.toString() + " Value: "+ mapa.get(llave));
        }
    }
    public void llenarLista(Persona ... persona) throws Excepcion40 {
        for ( Persona item: persona) {
            if( item.edad<=40){
                throw new Excepcion40();
            }
            lista.add(item);
        }
    }
    private void llenarMapa(){
        for ( Persona item: lista) {
            if( mapa.containsKey(item)){
                mapa.replace(item, mapa.get(item) + 1);
            }
            else{
                mapa.put(item, 1);
            }
        }
    }
}

