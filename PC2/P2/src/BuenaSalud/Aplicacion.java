package BuenaSalud;

public class Aplicacion {


    public static void main(String[] args) {
        Personas personas = new Personas();
        Persona persona1 =new Persona("Misshel",41); //solo se agrega los otros datos que ya se encuentran inicializados
        System.out.println("Inicio del proceso");
        try{
            personas.llenarLista(persona1);
        }catch (Excepcion40 e){
            e.printStackTrace();
        }
        finally {
            personas.procesarMapa();
        }
        System.out.println("Fin del proceso");

    }

}
