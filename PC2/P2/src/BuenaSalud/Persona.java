package BuenaSalud;

public class Persona {
    Integer dni;
    Integer edad;
    String nombre1;
    String nombre2;
    String apellidop;
    String apellidom;
    final Integer veces=1;

    public Persona(String nombre1, Integer edad) {
        this.dni = dni;
        this.edad =edad;
        this.nombre1 = nombre1;
        this.nombre2 = nombre2;
        this.apellidop = apellidop;
        this.apellidom = apellidom;
    }

    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getApellidop() {
        return apellidop;
    }

    public void setApellidop(String apellidop) {
        this.apellidop = apellidop;
    }

    public String getApellidom() {
        return apellidom;
    }

    public void setApellidom(String apellidom) {
        this.apellidom = apellidom;
    }
}
