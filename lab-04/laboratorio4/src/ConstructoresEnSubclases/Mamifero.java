package ConstructoresEnSubclases;

public class Mamifero extends Animal {
    public static String mamifero;
    public Mamifero(String sexo, String tamaño) {
        super(sexo, tamaño);
        this.mamifero="mamifero";
    }
    public String reproducirse() {
        return "Las hembras poseen glándulas mamarias.";
    }
}
