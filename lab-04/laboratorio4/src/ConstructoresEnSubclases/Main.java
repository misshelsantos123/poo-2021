package ConstructoresEnSubclases;

public class Main {
    public static void main(String[] args) {
        Mamifero humano = new Mamifero("hembra","promedio");
        Mamifero perro = new Mamifero("macho","pequeño");
        Animal gato = new Animal("hembra","grande");
        System.out.println("El tamaño del perro es: "+perro.Tamaño);
        System.out.println("Clase: "+Animal.animal);
        System.out.println("Clase: "+Mamifero.mamifero);
        System.out.println(humano.reproducirse());
        System.out.println(gato.reproducirse());
    }

}
