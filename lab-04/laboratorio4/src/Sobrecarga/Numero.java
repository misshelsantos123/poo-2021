package Sobrecarga;

public class Numero {
    //Lo distingue por el tipo de parametro
    public static void operarion(int numero1, int numero2) {
        System.out.println(numero1 + numero2);
    }

    public static void operacion(double numero1, double numero2) {
        System.out.println(numero1 / numero2);
    }

    public static void operacion(long numero1, long numero2) {
        System.out.println(numero1 - numero2);
    }

    public static void operacion(float numero1, float numero2) {
        System.out.println(numero1 * numero2);
    }
}

