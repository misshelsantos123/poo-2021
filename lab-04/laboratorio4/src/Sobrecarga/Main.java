package Sobrecarga;

public class Main {
    public static void main(String[] args) {
        int n1 = 20;
        int n2 = 15;
        Numero.operacion(n1, n2);

        double n3 = 27d;
        double n4 = 3d;
        Numero.operacion(n3, n4);

        long n5 = 26;
        long n6 = 14;
        Numero.operacion(n5, n6);

        float n7 = 12;
        float n8 = 4;
        Numero.operacion(n7, n8);
    }

}
