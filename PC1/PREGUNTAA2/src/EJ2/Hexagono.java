package EJ2;

public class Hexagono {
    double lado;
    public Hexagono (double lado){
        this.lado=lado;
    }

    public double getPerimetro (double lado){
        double perimetro=6*lado;
        return perimetro;
    }

    public double getArea (double lado){
        double area=2.59807*(lado*lado);
        return area;
    }
    public int getDiagonal(){
        return 9;
    }
}
