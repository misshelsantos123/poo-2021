/*Un estudiante de la facultad requiere crear un programa que le permita calcular el área, perímetro, cantidad de
diagonales de los polígonos regulares con cantidad de lados menores a 7. sabiendo la longitud del lado.
Ayude al estudiante a realizar los cálculos usando clases: atributos y métodos. Considerar métodos estáticos y no estáticos.
 */

package EJ2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int dato;
        double lado;
        System.out.println("INGRESE LA CANTIDAD DE LADOS DEL POLIGONO REGULAR MENOR A 7: ");
        dato=entrada.nextInt();
        System.out.println("INGRESE LA LONGITUD DE SU LADO: ");
        lado=entrada.nextDouble();
        switch(dato) {
            case 3:
                Triangulo t = new Triangulo(lado);
                System.out.println("El poligono es un triangulo de perimetro " + t.getPerimetro(lado) + " y area " + t.getArea(lado) + " y " + t.getDiagonal() + " diagonales");
                break;
            case 4:
                Cuadrado c = new Cuadrado(lado);
                System.out.println("El poligono es un cuadrado de perimetro " + c.getPerimetro(lado) + " y area " + c.getArea(lado) + " y "+c.getDiagonal()+" diagonales");
                break;
            case 5:
                Pentagono p = new Pentagono(lado);
                System.out.println("El poligono es un pentagono de perimetro " + p.getPerimetro(lado) + " y area " + p.getArea(lado) + " y "+p.getDiagonal()+" diagonales");
                break;
            case 6:
                Hexagono h = new Hexagono(lado);
                System.out.println("El poligono es un hexagono de perimetro " + h.getPerimetro(lado) + " y area " + h.getArea(lado) + " y "+h.getDiagonal()+" diagonales");
                break;
            default:
                System.out.println("NO ES POLIGONO REGULAR");
            }
        }
    }

