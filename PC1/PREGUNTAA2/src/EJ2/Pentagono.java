package EJ2;

public class Pentagono {
    double lado;
    public Pentagono (double lado){
        this.lado=lado;
    }

    public double getPerimetro (double lado){
        double perimetro=5*lado;
        return perimetro;
    }

    public double getArea (double lado){
        double area=(1.37638*(lado*lado))/2;
        return area;
    }
    public int getDiagonal(){
        return 5;
    }
}
