package EJ2;

public class Triangulo {
    double lado;
    public Triangulo (double lado){
        this.lado=lado;
    }

    public double getPerimetro (double lado){
        double perimetro=3*lado;
        return perimetro;
    }

    public double getArea (double lado){
        double area=(lado*lado/4)*1.73205;
        return area;
    }
    public int getDiagonal(){
        return 0;
    }
}
