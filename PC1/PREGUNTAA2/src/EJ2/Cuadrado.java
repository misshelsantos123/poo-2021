package EJ2;

public class Cuadrado {
    double lado;
    public Cuadrado (double lado){
        this.lado=lado;
    }

    public double getPerimetro (double lado){
        double perimetro=4*lado;
        return perimetro;
    }

    public double getArea (double lado){
        double area=lado*lado;
        return area;
    }
    public int getDiagonal(){
        return 2;
    }
}
