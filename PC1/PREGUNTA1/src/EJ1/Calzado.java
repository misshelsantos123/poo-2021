package EJ1;

public class Calzado {
    String marca;
    String modelo;
    String tipo;
    String genero;
    String horma;
    String material;
    String temporada;
    String pais;


    public Calzado (String marca, String modelo, String tipo, String genero, String horma, String material, String temporada, String pais){
        this.marca=marca;
        this.modelo=modelo;
        this.tipo=tipo;
        this.genero=genero;
        this.horma=horma;
        this.material=material;
        this.temporada=temporada;
        this.pais=pais;
    }
    public String getCalzado (){
        return "El calzado tiene la marca "+marca+", el modelo "+modelo+", el tipo "+tipo+", el genero "+género+", la horma "+horma+", el material"+material+", la temporada "+temporada+"y el pais de origen "+pais+"\n";
    }

}
