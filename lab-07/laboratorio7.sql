
CREATE DATABASE poo;

USE poo;
--Escriba una tabla producto con los siguientes campos: código de producto, nombre,
--descripción, precio unitario
CREATE TABLE producto(
                         codigo_producto NUMERIC(3,0) PRIMARY key,
                         nombre VARCHAR(100),
                         descripcion VARCHAR(1000),
                         precio_unitario NUMERIC(8,2)
);

--Asigne cinco registros a la tabla de productos
INSERT INTO producto(codigo_producto, nombre, descripcion, precio_unitario)
VALUES(1, 'Tarjeta de sonido Sony 3000', 'Tarjeta de sonido Sony 300. Conector RCA.', 250.00);
INSERT INTO producto(codigo_producto, nombre, descripcion, precio_unitario)
VALUES(2, 'Toalla', 'Toalla de algodón 300 gr', 25.50);
INSERT INTO producto(codigo_producto, nombre, descripcion, precio_unitario)
VALUES(3, 'Cuaderno espiralado', 'Cuaderno espiralado A4.', 10.00);
INSERT INTO producto(codigo_producto, nombre, descripcion, precio_unitario)
VALUES(4, 'Escalímetro', 'Escalímetro Artesco de 50 cm', 50.00);
INSERT INTO producto(codigo_producto, nombre, descripcion, precio_unitario)
VALUES(5, 'Tablet Samsung A1', 'Tableta Samsung A1 para diseñador gráficos', 750.00);

SELECT *
FROM producto;

--Modifique el nombre y precio unitario de dos productos usando código de producto.
--Elimine el cuarto producto

UPDATE producto
SET nombre = 'Compas', precio_unitario = 10.00
WHERE codigo_producto = 4;

UPDATE producto
SET nombre = 'Toalla antialergica', precio_unitario = 50.00
WHERE codigo_producto = 2;

DELETE FROM producto
WHERE codigo_producto = 4;


--Realice las siguientes consultas:
--• Mostrar los códigos y nombre de productos.
--• Mostrar el máximo y mínimo precio unitario.
--• Mostrar los productos que contengan la letra e.
--• Mostrar los productos ordenados por nombre

SELECT codigo_producto, nombre
FROM producto;

SELECT MAX(precio_unitario), MIN(precio_unitario)
FROM producto;

SELECT codigo_producto, nombre, descripcion, precio_unitario
FROM producto
WHERE nombre LIKE '%e%';

SELECT codigo_producto, nombre, descripcion, precio_unitario
FROM producto
ORDER BY nombre ASC;
