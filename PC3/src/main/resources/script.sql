create table pc3_departamento(
    codigo_departamento VARCHAR(10) PRIMARY key,
    nombre VARCHAR(100),
    ubicacion VARCHAR(50)
);
create table pc3_empleado(
    codigo_empleado VARCHAR(20) PRIMARY key,
    codigo_departamento VARCHAR(10),
    nombres VARCHAR(50) ,
    apellidos VARCHAR(100)
);
create table pc3_asignacion(
    id_asignacion NUMERIC(5) PRIMARY key,
    codigo_empleado VARCHAR(20),
    id_actividad NUMERIC(4),
    presupuesto NUMERIC (9,2)
);
create table pc3_actividad(
    id_actividad NUMERIC(4) PRIMARY key,
    nombre VARCHAR(50),
    prioridad NUMERIC(1)
);
-- INSERT INTO
insert into pc3_departamento(codigo_departamento, nombre, ubicacion)
VALUES (1,'Loreto','Selva');
insert into pc3_departamento(codigo_departamento, nombre, ubicacion)
VALUES (2,'Lima','Sierra');

insert into pc3_empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES (1,1,'Misshel','Santos');
insert into pc3_empleado(codigo_empleado, codigo_departamento, nombres, apellidos)
VALUES (2,2,'Danery','Alcarraz');

insert into pc3_asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
values (1,1,1,100);
insert into pc3_asignacion(id_asignacion, codigo_empleado, id_actividad, presupuesto)
values (2,2,2,200);

insert into pc3_actividad(id_actividad, nombre, prioridad)
values (1,'Limpiar',1);
insert into pc3_actividad(id_actividad, nombre, prioridad)
values (2,'Vender',2);


-- Obtener los nombres, apellidos y el nombre del departamento de todos los empleados.
select e.nombres, e.apellidos, d.nombre
from pc3_empleado e
join pc3_departamento d on (e.codigo_departamento = d.codigo_departamento);
-- Registrar una nueva actividad.
insert into pc3_actividad(id_actividad, nombre, prioridad)
VALUES (3,'Hablar',3);
-- Obtener las asignaciones de un determinado empleado.
select id_asignacion, codigo_empleado, id_actividad, presupuesto
from pc3_asignacion
where codigo_empleado = 1;
-- Actualizar la ubicación de un determinado departamento
update pc3_departamento
set ubicacion = 'Costa'
WHERE codigo_departamento = 2;