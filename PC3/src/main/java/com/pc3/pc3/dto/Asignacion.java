package com.pc3.pc3.dto;

import lombok.Data;

@Data

public class Asignacion {
    private Integer id_asignacion;
    private String codigo_empleado;
    private Integer id_actividad;
    private Integer presupuesto;
}
