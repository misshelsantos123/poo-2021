package com.pc3.pc3.dto;

import lombok.Data;

@Data
public class Actividad {
    private Integer id_actividad;
    private String nombre;
    private Integer prioridad;

    public Actividad(Integer id_actividad, String nombre, Integer prioridad) {
        this.id_actividad = id_actividad;
        this.nombre = nombre;
        this.prioridad = prioridad;
    }
}
