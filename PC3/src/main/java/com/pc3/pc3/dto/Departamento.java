package com.pc3.pc3.dto;

import lombok.Data;

@Data

public class Departamento {
    private String codigo_departamento;
    private String nombre;
    private String ubicacion;

    public Departamento(String codigo_departamento, String nombre, String ubicacion) {
        this.codigo_departamento = codigo_departamento;
        this.nombre = nombre;
        this.ubicacion = ubicacion;
    }
}
