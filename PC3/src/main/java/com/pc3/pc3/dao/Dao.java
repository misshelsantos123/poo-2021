package com.pc3.pc3.dao;
import com.pc3.pc3.dto.Actividad;
import com.pc3.pc3.dto.Asignacion;
import com.pc3.pc3.dto.Departamento;
import com.pc3.pc3.dto.Empleado;
import java.util.List;
public interface Dao {
    // Obtener los nombres, apellidos y el nombre del departamento de todos los empleados.
    public List<Empleado> obtenerEmpleados();
    //Registrar una nueva actividad.
    public void agregarActividad(Actividad actividad);
    //Obtener las asignaciones de un determinado empleado.
    public List<Asignacion> obtenerAsignaciones(Empleado empleado);
    //Actualizar la ubicación de un determinado departamento
    public void actualizarUbicacion(Departamento departamento);
}
