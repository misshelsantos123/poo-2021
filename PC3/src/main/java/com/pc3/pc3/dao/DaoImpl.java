package com.pc3.pc3.dao;

import com.pc3.pc3.dto.Actividad;
import com.pc3.pc3.dto.Asignacion;
import com.pc3.pc3.dto.Departamento;
import com.pc3.pc3.dto.Empleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class DaoImpl implements Dao{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private Connection conexion;
    private void crearConexion(){
        try {
            conexion = jdbcTemplate.getDataSource().getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
    private void cerrarConexion(){
        try {
            conexion.commit();
            conexion.close();
            conexion = null;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    public List<Empleado> obtenerEmpleados() {
        List<Empleado> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select e.nombres, e.apellidos, d.nombre").
                append(" from pc3_empleado e").
                append(" join pc3_departamento d on (e.codigo_departamento = d.codigo_departamento)");
        crearConexion();
        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sb.toString());
            while(resultado.next()){
                Empleado empleado = new Empleado();
                empleado.setCodigo_empleado(resultado.getString("codigo_empleado"));
                empleado.setCodigo_empleado(resultado.getString("codigo_departamento"));
                empleado.setCodigo_empleado(resultado.getString("codigo_nombres"));
                empleado.setCodigo_empleado(resultado.getString("codigo_apellidos"));
                lista.add(empleado);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }

    public void agregarActividad(Actividad actividad) {
        List<Actividad> actividades = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" insert into pc3_actividad(id_actividad, nombre, prioridad)").
                append( "VALUES (3,'Hablar',3);");
        crearConexion();
        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sb.toString());
            while(resultado.next()){
                Actividad actividad1 = new Actividad(resultado.getInt("id_actividad"),
                        resultado.getString("nombre"),
                        resultado.getInt("prioridad"));
                actividades.add(actividad1);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public List<Asignacion> obtenerAsignaciones(Empleado empleado) {
        List<Asignacion> lista = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" select id_asignacion, codigo_empleado, id_actividad, presupuesto").
                append(" from pc3_asignacion").
                append( "where codigo_empleado = ?");
        crearConexion();
        try {
            PreparedStatement sentencia = conexion.prepareStatement(sb.toString());
            sentencia.setString(1, empleado.getCodigo_empleado());
            ResultSet resultado = sentencia.executeQuery();

            while(resultado.next()){
                Asignacion asignacion = new Asignacion();
                asignacion.setId_asignacion(resultado.getInt("id_asignacion"));
                asignacion.setCodigo_empleado(resultado.getString("codigo_empleado"));
                asignacion.setId_actividad(resultado.getInt("id_actividad"));
                asignacion.setPresupuesto(resultado.getInt("presupuesto"));
                lista.add(asignacion);
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return lista;
    }
    public void actualizarUbicacion(Departamento departamento) {
        List<Departamento> departamentos = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append(" update pc3_departamento").
                append(" set ubicacion = 'Costa' ").
                append( "WHERE codigo_departamento = 2");
        crearConexion();
        try {
            Statement sentencia = conexion.createStatement();
            ResultSet resultado = sentencia.executeQuery(sb.toString());
            while(resultado.next()){
                Departamento departamento1 = new Departamento(resultado.getString("codigo_departamento"),resultado.getString("nobmbre"),resultado.getString("ubicacion"));

                if(departamento1.getCodigo_departamento()=="Sierra"){
                    departamento1.setUbicacion("Costa");
                }
            }
            resultado.close();
            sentencia.close();
            cerrarConexion();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
