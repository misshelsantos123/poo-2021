package com.pc3.pc3.servicio;

import com.pc3.pc3.dto.Actividad;
import com.pc3.pc3.dto.Asignacion;
import com.pc3.pc3.dto.Departamento;
import com.pc3.pc3.dto.Empleado;

import java.util.List;

public interface Servicio {
    public List<Empleado> obtenerEmpleados();
    public void agregarActividad(Actividad actividad);
    public List<Asignacion> obtenerAsignaciones(Empleado empleado);
    public void actualizarUbicacion(Departamento departamento);
}
