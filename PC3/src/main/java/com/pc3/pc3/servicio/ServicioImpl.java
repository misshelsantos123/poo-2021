package com.pc3.pc3.servicio;

import com.pc3.pc3.dao.Dao;
import com.pc3.pc3.dto.Actividad;
import com.pc3.pc3.dto.Asignacion;
import com.pc3.pc3.dto.Departamento;
import com.pc3.pc3.dto.Empleado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ServicioImpl implements Servicio{
    @Autowired
    private Dao dao;

    @Override
    public List<Empleado> obtenerEmpleados() {
        List<Empleado> lista= new ArrayList<>();
        return lista;
    }

    @Override
    public void agregarActividad(Actividad actividad) {
    }

    @Override
    public List<Asignacion> obtenerAsignaciones(Empleado empleado) {
        List<Asignacion> lista= new ArrayList<>();
        return lista;
    }

    @Override
    public void actualizarUbicacion(Departamento departamento) {
    }
}
