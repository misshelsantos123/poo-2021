package Parte1.Producto;

public class Fruta {
    protected String nombre;
    protected Double kilos;
    protected Double precioFruta;

    public Fruta(String nombre, Double kilos, Double precioFruta) {
        this.nombre = nombre;
        this.kilos = kilos;
        this.precioFruta= precioFruta;
    }

    public Double obtenerMonto() {
        return this.kilos * this.precioFruta;
    }

    public void mostrarDatos() {
        System.out.println(this.kilos + " kilogramos de "+ this.nombre+".");
    }
}

