package Parte1.Producto;

public class Manzana extends Fruta{
    String color;
    public Manzana(Double kilos, String color) {
        super("Manzana", kilos, 2.0d);
        this.color = color;
    }
    public void mostrarDatos() {
        System.out.println(this.kilos + " kilogramos de "+ this.nombre+this.color+".");
    }
}
