package Parte1.PedidosDeTransporte;
import java.util.*;
import Parte1.Producto.*;

public class Main {
    public static void main(String[] args) {
        Cliente cliente1 = new Cliente("Misshel","SMP");
        List<Fruta> frutas = new ArrayList<>();
        Pedido pedido1 = new Pedido(cliente1, frutas);

        Fruta manzana = new Manzana (20.0d, "verde");
        Fruta naranja = new Naranja(30.0d);
        frutas.add(manzana);
        frutas.add(naranja);

        manzana.mostrarDatos();
        naranja.mostrarDatos();
        pedido1.getMontoTotal();
        System.out.println("Direccion: "+pedido1.cliente.direcccionEntrega);
        System.out.println("Transporte: "+Transporte.transporteTerreste);
    }
}


