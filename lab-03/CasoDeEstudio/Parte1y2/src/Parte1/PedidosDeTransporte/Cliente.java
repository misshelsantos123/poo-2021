package Parte1.PedidosDeTransporte;

public class Cliente {
    protected String nombre;
    protected String direcccionEntrega;

    public Cliente(String nombre, String direcccionEntrega) {
        this.nombre = nombre;
        this.direcccionEntrega = direcccionEntrega;
    }
}
