package Parte1.PedidosDeTransporte;
import java.util.*;
import Parte1.Producto.*;

public class Pedido {
    protected Cliente cliente;
    protected List<Fruta> frutas = new ArrayList<Fruta>();
    protected Double montoTotal;

    public Pedido(Cliente cliente, List<Fruta> frutas) {
        this.cliente = cliente;
        this.frutas = frutas;
    }

    public void getMontoTotal() {
        Double sumaMonto=0d;
        for (Fruta fruta:frutas) {
            sumaMonto = Double.sum(sumaMonto, fruta.obtenerMonto());
        }
        this.montoTotal = sumaMonto;
        System.out.println("El monto total a pagar es: " + this.montoTotal);
    }

}
