public class Main {
    public static void main(String[] args) {
        Curso poo = new Curso(2, 12, "101");
        Alumno alumno1 = new Alumno(new double[]{20.0, 16.0, 13.0}, "Misshel", "Santos");
        Alumno alumno2 = new Alumno(new double[]{19.0, 11.0, 10.0}, "Carlos", "Carlin");
        Alumno[] alumnado = {alumno1,alumno2};
        Sesion sesionPoo = new Sesion(alumnado, poo);
        System.out.println("La segunda nota de misshel en el curso de POO es: "+sesionPoo.alumnos[0].getNotas()[1]);
    }


}
