public class Sesion {
    protected Alumno[] alumnos;
    protected Curso curso;

    public Sesion(Alumno[] alumnos, Curso curso) {
        this.alumnos = alumnos;
        this.curso = curso;
    }

    public Alumno[] getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(Alumno[] alumnos) {
        this.alumnos = alumnos;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

}
