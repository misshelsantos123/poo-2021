public class Curso {
    private Integer creditos;
    private Integer horasTotales;
    private String codigo;
    public Curso(Integer creditos, Integer horasTotales, String codigo) {
        this.creditos = creditos;
        this.horasTotales = horasTotales;
        this.codigo = codigo;
    }

    public Integer getCreditos() {
        return creditos;
    }

    public void setCreditos(Integer creditos) {
        this.creditos = creditos;
    }

    public Integer getHorasTotales() {
        return horasTotales;
    }

    public void setHorasTotales(Integer horasTotales) {
        this.horasTotales = horasTotales;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

}
