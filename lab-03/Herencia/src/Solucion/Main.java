package Solucion;

public class Main {
    public static void main(String[] args) {
        MotorGenerico motor = new MotorGenerico("Toyota", 12, 16, 20, 25);
        MotorFluvial motorF = new MotorFluvial("Nisan", 16, 20, 100, 150, 200);
        motor.transformar();
        motorF.transformar();
        motorF.producir();
    }
}
