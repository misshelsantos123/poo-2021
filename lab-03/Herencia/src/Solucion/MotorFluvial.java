package Solucion;

public class MotorFluvial extends MotorGenerico {
    private int costo;

    public MotorFluvial(String marca, int rendimiento, int velocidadNominal, int potencia, int estabilidad, int costo) {
        super(marca, rendimiento, velocidadNominal, potencia, estabilidad);
        this.costo = costo;
    }
    @Override
    public void transformar(){
        System.out.println("Tranforma energía para mover agua.");
    }
    @Override
    public void producir (){
        System.out.println("Produce movimiento con agua.");
    }
}
