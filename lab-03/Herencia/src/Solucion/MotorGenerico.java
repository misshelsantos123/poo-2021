package Solucion;

public class MotorGenerico {
    protected String marca;
    protected int rendimiento;
    protected int velocidadNominal;
    protected int potencia;
    protected int estabilidad;

    public MotorGenerico(String marca, int rendimiento, int velocidadNominal, int potencia, int estabilidad){
        this.marca = marca;
        this.rendimiento = rendimiento;
        this.velocidadNominal = velocidadNominal;
        this.potencia = potencia;
        this.estabilidad = estabilidad;
    }

    public void funcionar(){
        System.out.println("Hace funcionar el sistema.");
    }
    public void transformar(){
        System.out.println("Transforma energía.");
    }
    public void producir(){
        System.out.println("Produce movimiento.");
    }
}
