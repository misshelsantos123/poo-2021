import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        double[] coeficientes = new double[3];
        double[] soluciones = new  double[2];
        EcuacionSegundoGrado ecuacionSegundoGrado = new EcuacionSegundoGrado(soluciones, coeficientes);
        for(int i=0;i<3;i++){
            System.out.println("Ingrese coeficiente "+(i+1)+" :");
            ecuacionSegundoGrado.coeficientes[i]=entrada.nextDouble();
        }
        ecuacionSegundoGrado.hallarSolucion();
        ecuacionSegundoGrado.mostrarRaices();
    }
}
