public class EcuacionSegundoGrado extends Ecuacion{
    public EcuacionSegundoGrado(double[] soluciones, double[] coeficientes) {
        super(soluciones, coeficientes);
    }
    @Override
    public double[] hallarSolucion() {
        soluciones[0]=((-1*coeficientes[1])+Math.sqrt(coeficientes[1]*coeficientes[1]-4*coeficientes[0]*coeficientes[2]))/(2*coeficientes[0]);
        soluciones[1]=((-1*coeficientes[1])-Math.sqrt(coeficientes[1]*coeficientes[1]-4*coeficientes[0]*coeficientes[2]))/(2*coeficientes[0]);
        return soluciones;
    }
    public void mostrarRaices (){
        hallarSolucion();
        for(int i=0;i<2;i++){
            System.out.println(soluciones[i]);
        }
    }

}
