public abstract class Ecuacion {
    protected double [] soluciones;
    protected double [] coeficientes;
    public Ecuacion(double [] soluciones, double [] coeficientes) {
        this.soluciones = soluciones;
        this.coeficientes = coeficientes;
    }
    public abstract double[] hallarSolucion();
}
