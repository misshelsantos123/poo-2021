package uni.fiis.laboratorio2.pregunta3;

import java.util.Scanner;

public class CifradoCesar {
    private String mensaje;
    private String mensajeCodificado = "";
    private String mensajeDecodificado = "";

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        CifradoCesar oracion = new CifradoCesar();

        System.out.print("Ingrese un mensaje: ");
        oracion.setMensaje(entrada.nextLine());
        oracion.codificar();

        CifradoCesar oracionCodificada = new CifradoCesar();
        oracionCodificada.setMensaje(oracion.getMensajeCodificado());
        oracionCodificada.decodificar();
    }

    public void codificar() {
        for (int i = 0; i < mensaje.length(); i++) {
            char caracter;
            if (mensaje.charAt(i) >= 'A' && mensaje.charAt(i) <= 'W') {
                caracter = mensaje.charAt(i);
                caracter += 3;
                mensajeCodificado += caracter;
            } else if (mensaje.charAt(i) >= 'X' && mensaje.charAt(i) <= 'Z') {
                caracter = mensaje.charAt(i);
                caracter -= 23;
                mensajeCodificado += caracter;
            }
        }
        System.out.println("Mensaje codificado: " + mensajeCodificado);
    }

    public void decodificar() {
        for (int i = 0; i < mensaje.length(); i++) {
            char caracter;
            if (mensaje.charAt(i) >= 'D' && mensaje.charAt(i) <= 'Z') {
                caracter = mensaje.charAt(i);
                caracter -= 3;
                mensajeDecodificado += caracter;
            } else if (mensaje.charAt(i) >= 'A' && mensaje.charAt(i) <= 'C') {
                caracter = mensaje.charAt(i);
                caracter += 23;
                mensajeDecodificado += caracter;
            }
        }
        System.out.println("Mensaje decodificado: " + mensajeDecodificado);
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getMensajeCodificado() {
        return mensajeCodificado;
    }

    public void setMensajeCodificado(String mensajeCodificado) {
        this.mensajeCodificado = mensajeCodificado;
    }

    public String getMensajeDecodificado() {
        return mensajeDecodificado;
    }

    public void setMensajeDecodificado(String mensajeDecodificado) {
        this.mensajeDecodificado = mensajeDecodificado;
    }

}
