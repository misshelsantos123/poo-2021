package uni.fiis.laboratorio2.pregunta2;

import java.util.Scanner;

public class Circulo {
    private Double radio;

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        Circulo circulo1 = new Circulo();
        System.out.println("Ingrese el radio: ");
        circulo1.setRadio(entrada.nextDouble());

        System.out.print("El area del circulo es: " + Circulo.hallarAreaCirculo(circulo1.getRadio()));

    }

    public Double getRadio() {
        return radio;
    }

    public void setRadio(Double radio) {
        this.radio = radio;
    }

    public static Double hallarAreaCirculo(Double radio) { // Metodo estatico
        return Math.PI * Math.pow(radio, 2);
    }
}
