package uni.fiis.laboratorio2.pregunta1;

import java.lang.Math;
import java.util.Scanner;

public class EcuacionSegundoGrado {
    private Integer coeficiente1;
    private Integer coeficiente2;
    private Integer coeficiente3;
    private Double primeraRaiz;
    private Double segundaRaiz;

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        EcuacionSegundoGrado ecuacion = new EcuacionSegundoGrado();
        System.out.println("Primer coeficiente: ");
        ecuacion.setCoeficiente1(entrada.nextInt());
        System.out.println("Segundo coeficiente: ");
        ecuacion.setCoeficiente2(entrada.nextInt());
        System.out.println("Tercer coeficiente: ");
        ecuacion.setCoeficiente3(entrada.nextInt());

        System.out.println("Las raices de la ecuacion son:");
        ecuacion.hallarRaices();

    }

    public void hallarRaices() {
        primeraRaiz = (-coeficiente2 + Math.pow(coeficiente2 * coeficiente2 - 4 * coeficiente1 * coeficiente3, 0.5))
                / (2 * coeficiente1);
        segundaRaiz = (-coeficiente2 - Math.pow(coeficiente2 * coeficiente2 - 4 * coeficiente1 * coeficiente3, 0.5))
                / (2 * coeficiente1);

        System.out.println("Primera raiz: " + primeraRaiz);
        System.out.println("Segunda raiz: " + segundaRaiz);
    }

    public Integer getCoeficiente1() {
        return coeficiente1;
    }

    public void setCoeficiente1(Integer coeficiente1) {
        this.coeficiente1 = coeficiente1;
    }

    public Integer getCoeficiente2() {
        return coeficiente2;
    }

    public void setCoeficiente2(Integer coeficiente2) {
        this.coeficiente2 = coeficiente2;
    }

    public Integer getCoeficiente3() {
        return coeficiente3;
    }

    public void setCoeficiente3(Integer coeficiente3) {
        this.coeficiente3 = coeficiente3;
    }

    public Double getPrimeraRaiz() {
        return primeraRaiz;
    }

    public void setPrimeraRaiz(Double primeraRaiz) {
        this.primeraRaiz = primeraRaiz;
    }

    public Double getSegundaRaiz() {
        return segundaRaiz;
    }

    public void setSegundaRaiz(Double segundaRaiz) {
        this.segundaRaiz = segundaRaiz;
    }
}

