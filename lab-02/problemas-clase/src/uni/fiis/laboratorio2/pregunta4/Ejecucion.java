package uni.fiis.laboratorio2.pregunta4;

import cuadrilatero.Cuadrado;
import triangulo.Triangulo;

public class Ejecucion {
    public static void main(String[] args) {
        Cuadrado cuadrado1 = new Cuadrado();
        cuadrado1.setLado(3D);
        cuadrado1.calcularArea();

        Triangulo triangulo1 = new Triangulo();
        triangulo1.setAltura(4D);
        triangulo1.setBase(5D);
        triangulo1.calcularArea();
    }
}
