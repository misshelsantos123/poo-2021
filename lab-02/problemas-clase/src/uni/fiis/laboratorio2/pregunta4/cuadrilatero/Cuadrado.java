
package cuadrilatero;

public class Cuadrado {
    private Double lado;

    public Double getLado() {
        return lado;
    }

    public void setLado(Double lado) {
        this.lado = lado;
    }

    public void calcularArea() {
        System.out.println("El area del cuadrado es: " + Math.pow(lado, 2));
    }
}

