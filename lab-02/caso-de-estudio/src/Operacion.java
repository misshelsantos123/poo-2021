public class Operacion {
    private Numero numero1;
    private Numero numero2;
    private Numero resultado;

    public static void main(String[] args) {
        Operacion operacion1 = new Operacion();

        Numero number1 = new Numero();
        number1.setNumero(-51111);
        number1.setBase(9);
        Numero number2 = new Numero();
        number2.setNumero(-2354);
        number2.setBase(9);
        Numero number3 = new Numero();
        number3.setBase(10);

        operacion1.setNumero1(number1);
        operacion1.setNumero2(number2);
        operacion1.setResultado(number3);

        System.out.println(
                "La suma en base " + operacion1.getResultado().getBase() + " es: " + operacion1.calcularSuma());

        System.out.println(
                "La resta en base " + operacion1.getResultado().getBase() + " es: " + operacion1.calcularResta());

    }

    public Integer calcularSuma() {
        numero1.cambiarBaseA(10);
        numero2.cambiarBaseA(10);

        Integer base = resultado.getBase();

        resultado.setBase(10);
        resultado.setNumero(numero1.getNumero() + numero2.getNumero());
        resultado.cambiarBaseA(base);

        return resultado.getNumero();

    }

    public Integer calcularResta() {
        numero1.cambiarBaseA(10);
        numero2.cambiarBaseA(10);

        Integer base = resultado.getBase();

        resultado.setBase(10);
        resultado.setNumero(numero1.getNumero() - numero2.getNumero());
        resultado.cambiarBaseA(base);

        return resultado.getNumero();
    }

    public Numero getNumero1() {
        return numero1;
    }

    public void setNumero1(Numero numero1) {
        this.numero1 = numero1;
    }

    public Numero getNumero2() {
        return numero2;
    }

    public void setNumero2(Numero numero2) {
        this.numero2 = numero2;
    }

    public Numero getResultado() {
        return resultado;
    }

    public void setResultado(Numero resultado) {
        this.resultado = resultado;
    }
}

