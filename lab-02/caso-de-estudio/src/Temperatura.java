public class Temperatura {
    private Numero magnitud;
    private String unidad;

    public static void main(String[] args) {
        Numero magnitud1 = new Numero();
        magnitud1.setNumero(-12);
        magnitud1.setBase(8);

        Temperatura medida1 = new Temperatura();
        medida1.setMagnitud(magnitud1);
        medida1.setUnidad("ºC");
        medida1.mostrar();

        medida1.convertirAKelvin();
        System.out.println("Despues de convertir:");
        medida1.mostrar();
    }

    public void mostrar() {
        System.out.println("Tenemos : " + magnitud.getNumero() + unidad + " en base " + magnitud.getBase());
    }

    public void convertirAKelvin() {
        Numero magnitudEnCelcius = new Numero();
        magnitudEnCelcius.setNumero(magnitud.getNumero());
        magnitudEnCelcius.setBase(magnitud.getBase());

        Numero convertidor = new Numero();
        convertidor.setNumero(273);
        convertidor.setBase(10);

        Operacion AKelvin = new Operacion();
        AKelvin.setNumero1(magnitudEnCelcius);
        AKelvin.setNumero2(convertidor);
        AKelvin.setResultado(magnitud);

        magnitud.setNumero(AKelvin.calcularSuma());
        unidad = "K";
    }

    public void convertirACelcius() {
        Numero magnitudEnKelvin = new Numero();
        magnitudEnKelvin.setNumero(magnitud.getNumero());
        magnitudEnKelvin.setBase(magnitud.getBase());

        Numero convertidor = new Numero();
        convertidor.setNumero(273);
        convertidor.setBase(10);

        Operacion ACelcius = new Operacion();
        ACelcius.setNumero1(magnitudEnKelvin);
        ACelcius.setNumero2(convertidor);
        ACelcius.setResultado(magnitud);

        magnitud.setNumero(ACelcius.calcularResta());
        unidad = "ºC";
    }

    public Numero getMagnitud() {
        return magnitud;
    }

    public void setMagnitud(Numero magnitud) {
        this.magnitud = magnitud;
    }

    public String getUnidad() {
        return unidad;
    }

    public void setUnidad(String unidad) {
        this.unidad = unidad;
    }
}
