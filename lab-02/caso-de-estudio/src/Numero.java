public class Numero {
    private Integer numero;
    private Integer base;

    public static void main(String[] args) { // Probando los metodos
        Numero NumeroA = new Numero();
        NumeroA.setNumero(-123);
        NumeroA.setBase(8);
        System.out.println("Numero: " + NumeroA.getNumero());
        System.out.println("Base: " + NumeroA.getBase());
        NumeroA.cambiarBaseA(5);
        System.out.println("Numero: " + NumeroA.getNumero());
        System.out.println("Base: " + NumeroA.getBase());
    }

    public void cambiarBaseA(Integer nuevaBase) {
        boolean signo = true;
        if (numero < 0) {
            numero = numero * -1;
            signo = false;
        }
        Integer auxiliar = numero;
        Integer numeroBase10 = 0;
        Integer i = 1;
        while (auxiliar > 0) {
            numeroBase10 += (auxiliar % 10) * i;
            auxiliar = auxiliar / 10;
            i = i * base;
        }
        numero = 0;
        i = 1;
        while (numeroBase10 > 0) {
            numero += (numeroBase10 % nuevaBase) * i;
            numeroBase10 = numeroBase10 / nuevaBase;
            i = i * 10;
        }
        if (!signo) {
            numero = numero * -1;
        }
        setNumero(numero);
        setBase(nuevaBase);
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getBase() {
        return base;
    }

    public void setBase(Integer base) {
        this.base = base;
    }

}
