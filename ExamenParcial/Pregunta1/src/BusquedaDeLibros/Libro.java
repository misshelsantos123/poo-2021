package BusquedaDeLibros;

public class Libro extends BuscarLibro{
    //Caracteristicas del libro
    protected String descripcion;
    protected String editorial;
    protected String anioDePublicacion;
    protected String tituloDeCapitulo;
    protected String resumen;


    public Libro(String titulo, String autor, String codigo) {
        this.titulo = titulo;
        this.autor = autor;
        this.codigo = codigo;
    }

    @Override
    public String busquedaPorTitulo() {
        return this.titulo;
    }

    @Override
    public String busquedaPorAutor() {
        return this.autor;
    }

    @Override
    public String busquedaPorCodigo() {
        return this.codigo;
    }
}
