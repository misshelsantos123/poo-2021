package BusquedaDeLibros;
import java.util.*;

public class Busqueda {
    public static void main(String[] args) {
        LinkedList<Libro> libros=new LinkedList<Libro>();
        /*Por tiempo solo defini los criterios necesarios para la prueba, en caso se quieran aumentar mas
        criterios se agregarian a la clase padre de libro BuscarLibro*/

        Libro libro1= new Libro("Seda","Alessandro","101");
        Libro libro2 =new Libro("Aura","Carlos","102");
        libros.add(libro1);
        libros.add(libro2);
        String autor= "Carlos";
        String codigo="101";

        for (int i=0;i<2;i++) {
            if(libros.get(i).busquedaPorAutor()==autor || libros.get(i).busquedaPorCodigo()==codigo){
                System.out.println("Se tiene el libro "+libros.get(i).busquedaPorTitulo()+" del autor "+libros.get(i).busquedaPorAutor()+" de codigo "+libros.get(i).busquedaPorCodigo());
            }
        }
    }
}
