package BusquedaDeLibros;

public abstract class BuscarLibro {
    //Aca se agregan mas criterios de busqueda
    protected String titulo;
    protected String autor;
    protected String codigo;
    public abstract String busquedaPorTitulo();
    public abstract String busquedaPorAutor();
    public abstract String busquedaPorCodigo();
}
