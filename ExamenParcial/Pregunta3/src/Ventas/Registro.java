package Ventas;

public class Registro {
    public Reloj reloj;
    public Integer cantidad;
    public Integer precioTotal;

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
    public void setPrecioTotal(Integer precioTotal) {
        this.precioTotal = this.reloj.precio*this.cantidad;
    }

    public Integer getPrecioTotal() {
        return precioTotal;
    }
}
