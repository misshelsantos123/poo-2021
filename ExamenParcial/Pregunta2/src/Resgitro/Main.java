package Resgitro;

public class Main {
    public static void main(String[] args) {
        Postulantes mapita = new Postulantes();
        Postulante postulante1 = new Postulante("101", "Misshel", -12d, NivelEducativo.SECUNDARIA);
        Postulante postulante2 = new Postulante("102", "Juan", 13d, NivelEducativo.SUPERIOR);
        System.out.println("Inicio del proceso");
        try {
            mapita.llenarLista(postulante1);
            mapita.llenarLista(postulante2);
        } catch (ExcepcionNegativo e) {
            e.printStackTrace();
        } finally {
            mapita.procesarMapa();
        }
        System.out.println("Fin del proceso");
    }
}
