package Resgitro;

public enum NivelEducativo {
    PRIMARIA("PRI"),SECUNDARIA("SEC"),SUPERIOR("SUP");

    private NivelEducativo(String abreviatura){
        this.abreviatura=abreviatura;
    }
    public String retornaAbreviatura(){
        return abreviatura;
    }
    private String abreviatura;
}
