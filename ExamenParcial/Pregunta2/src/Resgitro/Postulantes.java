package Resgitro;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Postulantes {
    List<Postulante> lista;
    Map<Postulante, Integer> mapa;

    public Postulantes() {
        this.lista = new ArrayList<Postulante>();
        this.mapa = new HashMap<Postulante, Integer>();
    }
    private void llenarMapa(){
        for ( Postulante item: lista) {
            if( mapa.containsKey(item)){
                mapa.replace(item, mapa.get(item)+1);
            }
            else{
                mapa.put(item, 1);
            }
        }
    }
    public void procesarMapa(){
        llenarMapa();
        mostrarDatosMapa();
    }
    private void mostrarDatosMapa(){
        for (Postulante llave: mapa.keySet()) {
            System.out.println("Postulante: "+ llave.nombre);
            System.out.println("Nota: "+ llave.modicarNota());
        }
    }
    public void llenarLista(Postulante... persona) throws ExcepcionNegativo {
        for ( Postulante item: persona) {
            if( item.nota<0){
                throw new ExcepcionNegativo();
            }
            lista.add(item);
        }
    }

}
