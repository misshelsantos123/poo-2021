package Resgitro;

public class Postulante implements Modificar {
    protected String DNI;
    protected String nombre;
    protected String apellido;
    protected String direccion;
    protected Integer telefono;
    protected Integer edad;
    protected Double nota=0d;
    protected NivelEducativo nivelEducativo;

    public Postulante(String DNI, String nombre, Double nota, NivelEducativo nivelEducativo) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.nota = nota;
        this.nivelEducativo = nivelEducativo;
    }


    public void establecerNota(Double nota){
        this.nota=nota;
    }

    @Override
    public double modicarNota() {
        return this.nota;
    }
}
